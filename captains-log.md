- A docker compose template was created to start up an demo environment.
- The agents need to be registered to be used.
- If you need change the name to an environment you must to delete it and recreate it.
- Plugins can't be installed via UI, JAR must be dropped into 
- Agents can be autoregistered following this procedure: https://docs.gocd.org/current/advanced_usage/agent_auto_register.html
- JVM values for agents can be modified using GOCD_AGENT_JVM_OPTS variable.
- Terraform provider can be found here: https://github.com/beamly/terraform-provider-gocd/releases, take into account is not a formal or supported by Hashicorp, `is only one person to mantain this provider`
- Agents base could be found here https://github.com/gocd/docker-gocd-agent-centos-7 
- Some interesting plugins:

|Name|Description|Type|Repo|
|---|---|---|---|
|Docker pipeline plugin|Builds a docker image from a Dockerfile, tags it and pushes it to a registry.|Task|https://github.com/Haufe-Lexware/gocd-plugins/wiki/Docker-plugin|
|Health Check|Task plugin for delaying the build till the application becomes healthy|Task|https://github.com/jmnarloch/gocd-health-check-plugin|
|Slack Task plugin|Send custom messages to Slack from Go CD jobs|Task|https://github.com/Vincit/gocd-slack-task/releases|

- To install a plugin put the `.jar` file into the folder godata/addons/ and restart the gocd-server.