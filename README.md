# GOCD Demo

## Requirements

- Docker desktop

## Start stack

In the root folder of this repo run the following command:

```
docker-compose up -d
```

## To check logs

```
docker-compose logs --follow --tail=100
```

## Demo application for testing propose

https://bitbucket.org/eidast/pdes-application.git

## Some useful plugins

|Name|Description|Type|Repo|
|---|---|---|---|
|Docker pipeline plugin|Builds a docker image from a Dockerfile, tags it and pushes it to a registry.|Task|https://github.com/Haufe-Lexware/gocd-plugins/wiki/Docker-plugin|
|Health Check|Task plugin for delaying the build till the application becomes healthy|Task|https://github.com/jmnarloch/gocd-health-check-plugin|
|Slack Task plugin|Send custom messages to Slack from Go CD jobs|Task|https://github.com/Vincit/gocd-slack-task/releases|